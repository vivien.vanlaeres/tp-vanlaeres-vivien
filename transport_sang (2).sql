-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 05 juin 2020 à 12:49
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `transport_sang`
--

-- --------------------------------------------------------

--
-- Structure de la table `container`
--

DROP TABLE IF EXISTS `container`;
CREATE TABLE IF NOT EXISTS `container` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numContainer` int(11) NOT NULL,
  `addrMac` varchar(25) NOT NULL,
  `etatContainer` enum('LIBRE','OCCUPE') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `numContainer` (`numContainer`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `container`
--

INSERT INTO `container` (`id`, `numContainer`, `addrMac`, `etatContainer`) VALUES
(1, 1, '30:AE:A4:FE:43:0E', 'OCCUPE'),
(4, 2, '25:GE:A4:68:3C:B8', 'LIBRE');

-- --------------------------------------------------------

--
-- Structure de la table `temperature`
--

DROP TABLE IF EXISTS `temperature`;
CREATE TABLE IF NOT EXISTS `temperature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temp` float NOT NULL,
  `idTemp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idTemp` (`idTemp`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `temperature`
--

INSERT INTO `temperature` (`id`, `temp`, `idTemp`) VALUES
(13, 6, 1),
(14, 8, 1),
(15, 6, 1),
(16, 7, 1),
(17, 15, 1),
(18, 6, 2),
(19, 4, 2),
(20, 6, 2),
(21, 7, 2),
(22, 5, 2);

-- --------------------------------------------------------

--
-- Structure de la table `transport`
--

DROP TABLE IF EXISTS `transport`;
CREATE TABLE IF NOT EXISTS `transport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numTransport` int(10) NOT NULL,
  `password` int(11) NOT NULL,
  `dateDepart` varchar(25) NOT NULL,
  `dateArrivee` varchar(25) NOT NULL,
  `idContainer` int(11) NOT NULL,
  `idTemp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idTemp` (`idTemp`),
  KEY `idContainer` (`idContainer`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `transport`
--

INSERT INTO `transport` (`id`, `numTransport`, `password`, `dateDepart`, `dateArrivee`, `idContainer`, `idTemp`) VALUES
(42, 2, 3245, '28/04/2020 13h35', '28/04/2020 14h00', 1, 1),
(44, 1, 1234, '20/06/2020 12h00', '20/03/2020 12h25', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `login` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `role` enum('infirmiere','operateur','hospitalier') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `login`, `password`, `role`) VALUES
(1, 'SNIR', 'Anne', 'anne.snir', 'anne59', 'infirmiere'),
(2, 'Dupont', 'Pierre', 'pierre.dupont', 'pierre59', 'operateur');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `temperature`
--
ALTER TABLE `temperature`
  ADD CONSTRAINT `temperature_ibfk_1` FOREIGN KEY (`idTemp`) REFERENCES `transport` (`idTemp`);

--
-- Contraintes pour la table `transport`
--
ALTER TABLE `transport`
  ADD CONSTRAINT `transport_ibfk_2` FOREIGN KEY (`idContainer`) REFERENCES `container` (`numContainer`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
