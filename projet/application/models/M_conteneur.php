<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_conteneur extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function select_user($prmid){
        
        $query = $this->db->select('*')
                          ->from('user')
                          ->where('login', $prmid)
                          ->get();
        return $query->result_array();
    }

    public function select_containers_by_mac($prmid){
        
        $query = $this->db->select('addrMac, etatContainer')
                          ->from('container')
                          ->where('addrMac', $prmid)
                          ->get();
        return $query->result_array();
    }

    public function select_containers(){
        
        $query = $this->db->select('*')
                          ->from('container')
                          ->get();
        return $query->result_array();

    }

    public function select_transport_by_conteneur($prmid){
        
        $query = $this->db->select('*')
                          ->from('container')
                          ->where('Id', $prmid)
                          ->get();
        return $query->result_array();
    }

    public function select_last_num_transport(){

        $query = $this->db->select('numTransport')
                          ->from('transport')
                          ->order_by('numTransport', 'DESC')
                          ->limit(1)
                          ->get();
        return $query->result_array();
    }

    public function select_uri_url_list($prmid){
        return $prmid;
    }

    public function select_transport_by_container($prmid){
        $query = $this->db->select('*')
                          ->from('transport')
                          ->where('idContainer', $prmid)
                          ->order_by('numTransport', 'DESC')
                          ->get();
        return $query->result_array();
    }

    public function select_detail_by_transport($prmid){
        $query = $this->db->select('*')
                          ->from('transport')
                          ->where('numTransport', $prmid)
                           ->get();
        return $query->result_array();
    }

    public function select_uri_url_detail($prmid){
        return $prmid;
    }

    public function insert_start($dto){
        $this->db->insert('transport', $dto);
    }

    public function select_idcontainer_by_addmaccontainer($prmid){
        $query = $this->db->select('numContainer')
                          ->from('container')
                          ->where('addrMac', $prmid)
                           ->get();
        return $query->result_array();
    }

    public function select_temperature($prmid){
        
        $query = $this->db->select('*')
                          ->from('temperature')
                          ->where('idTemp', $prmid)
                          ->get();
        return $query->result_array();
    }
    
    public function put_EtatContainer($prmid, $dto){
        $this->db->where('numContainer', $prmid)
                ->update('container', $dto);
        
    }

    public function select_last_num_idtemp(){

        $query = $this->db->select('idTemp')
                          ->from('transport')
                          ->order_by('idTemp', 'DESC')
                          ->limit(1)
                          ->get();
        return $query->result_array();
    }
    public function put_DateArrivee($prmid, $dto){
        $this->db->where('numTransport', $prmid)
                ->update('transport', $dto);
        
    }

}