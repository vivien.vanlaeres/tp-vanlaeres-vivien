<script type="text/javascript" src="<?php echo base_url("assets/js/sang.js"); ?>"></script>
<div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="mt-5">Connexion</h1>
        <p class="lead">Connectez-vous avec vos identifiants</p>
      <form>
        <div class="form-group">
          <label for="id">Identifiant</label>
          <input type="text" class="form-control" id="identifiant" aria-describedby="emailHelp" placeholder="Renseignez votre identifant">     <!-- Onglet identifiant  -->

        </div>
        <div class="form-group">
          <label for="mp">Mot de passe</label>
          <input type="password" class="form-control" id="motdepasse" placeholder="Renseignez votre mot de passe">  <!-- Onglet mot de passe   -->
        </div>
        <a id="btnConnexion" class="btn btn-primary">Connexion</a> <!-- Bouton Connexion  -->
        <p id="codErr"></p>
      </form>
      <br>
      <br>
      <img id="imglogo" src="<?php echo base_url("assets/img/chhazebrouck.png"); ?>" alt="logo" class="imgfloat" /> <!-- Image ch hazebrouck  -->
    </div>
    </div>
  </div>
