<script>
var numeroTransport = <?php echo json_encode($result); ?>;
</script>
<script type="text/javascript" src="<?php echo base_url("assets/js/html2pdf.bundle.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/Chart.bundle.min.js"); ?>"></script>
<div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
<form>
    <br>
    <!-- Div to capture. -->
    <div id="root">
    <img id="imglogopdf" src="<?php echo base_url("assets/img/chhazebrouck.png"); ?>" alt="logo" class="imgfloat" /> <!-- Image ch hazebrouck -->
    <h3>Numéro du transport : </h3><p id="numtransport"></p>
    <h3>Date de départ du container : </h3><p id="datedepart"></p>
    <h3>Date d'arrivée au laboratoire : </h3><p id="datearrivee"></p>
    <table id="divResult"> <!-- tableau de température -->
            <tr>
                <td id="titre">Température</td>
            </tr>
        </table>
    <canvas id="myChart"></canvas>


    </div>
    <button type="button" id="btnCreationPDF" class="btn btn-primary">Générer un PDF des résultats</button>
</form>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url("assets/js/transport_detail.js"); ?>"></script>
