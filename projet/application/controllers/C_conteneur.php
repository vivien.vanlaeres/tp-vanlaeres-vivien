<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_conteneur extends CI_Controller {
	public function __construct ()
	{
		CI_Controller::__construct();
		$this->load->helper('url');
	}


	public function index()
	{
	

		$page = $this->load->view('V_conteneur',"",true);

		$this->load->view('commun/V_template', array('contenue' => $page));
		
		$this->load->view('V_conteneur');
	}
}
