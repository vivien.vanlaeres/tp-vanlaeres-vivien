<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_transport extends CI_Controller {
	public function __construct ()
	{
		CI_Controller::__construct();
                $this->load->helper('url');
                $this->load->model('M_conteneur');

	}
	public function index()
	{
                $page = $this->load->view('V_transport',"",true);

                $this->load->view('commun/V_template', array('contenue' => $page));

		$this->load->view('V_transport');
        }
        public function list_transport($prmid) {
                $detail_resultat = $this->M_conteneur->select_uri_url_list($prmid);
                $data['result'] = $detail_resultat;
                $page = $this->load->view('V_transport',$data,true);

                $this->load->view('commun/V_template', array('contenue' => $page));

		$this->load->view('V_transport');
        }
        public function detail_transport($prmid) {
                $detail_resultat = $this->M_conteneur->select_uri_url_detail($prmid);
                $data['result'] = $detail_resultat;
                $page = $this->load->view('V_detail_transport',$data,true);

                $this->load->view('commun/V_template', array('contenue' => $page));

		$this->load->view('V_detail_transport');
        }
               
}


