<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Conteneur extends REST_Controller {
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('M_conteneur');
    }

    public function verifConnexion_get($id = ''){
        if ($id === ''){

        }else{
            $results = $this->M_conteneur->select_user($id);
            $this->response($results, REST_Controller::HTTP_OK);
            
        }
    }
    public function recupAddMacContainers_get($id = ''){
        if ($id === ''){

        }else{
            $results = $this->M_conteneur->select_containers_by_mac($id);
            $this->response($results, REST_Controller::HTTP_OK);
            
        }
    }

    public function recupContainers_get(){
        $results = $this->M_conteneur->select_containers();
        $this->response($results, REST_Controller::HTTP_OK);
                  
    }

    public function recupderniernumtransport_get(){
            $results = $this->M_conteneur->select_last_num_transport();
            $this->response($results, REST_Controller::HTTP_OK);
    }

    public function recupTransportByContainer_get($prmid = ''){
        if ($prmid === ''){

        }else{
            $results = $this->M_conteneur->select_transport_by_container($prmid);
            $this->response($results, REST_Controller::HTTP_OK);
            
        }
    }
    public function recupTransportDetail_get($prmid = ''){
        if ($prmid === ''){

        }else{
            $results = $this->M_conteneur->select_detail_by_transport($prmid);
            $this->response($results, REST_Controller::HTTP_OK);
            
        }
    }
    public function postDataDepartEnregistreur_post() {
        $dto = $this->_post_args['dto'];
        $dto = (array) json_decode($dto);

        $results = $this->M_conteneur->insert_start($dto);

        $this->response($results[0], REST_Controller::HTTP_CREATED);
    }
    public function recupIdContainerByAddMacContainer_get($prmid = ''){
        if ($prmid === ''){

        }else{
            $results = $this->M_conteneur->select_idcontainer_by_addmaccontainer($prmid);
            $this->response($results, REST_Controller::HTTP_OK);
        }
    }

    public function recupTemperature_get($prmid = ''){
        if ($prmid === ''){

        }else{
            $results = $this->M_conteneur->select_temperature($prmid);
            $this->response($results, REST_Controller::HTTP_OK);
        }
    }
    public function replaceEtatContainer_put($prmid = '') 
    {
        $dto = $this->_put_args['dto'];
        $dto = (array) json_decode($dto);

        $results = $this->M_conteneur->put_EtatContainer($prmid, $dto);
        
        $this->response($results[0], REST_Controller::HTTP_CREATED);
    }
    public function recupdernieridTemp_get(){
        $results = $this->M_conteneur->select_last_num_idtemp();
        $this->response($results, REST_Controller::HTTP_OK);
    }
    public function replaceDateArrivee_put($prmid = ''){
        $dto = $this->_put_args['dto'];
        $dto = (array) json_decode($dto);

        $results = $this->M_conteneur->put_DateArrivee($prmid, $dto);
        
        $this->response($results[0], REST_Controller::HTTP_CREATED);

    }

    //public function index_post(){}    
    //public function index_delete($id = ''){}
    //public function index_put($id = ''){}
}