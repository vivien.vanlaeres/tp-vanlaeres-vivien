    /* 
    * Fichier modèle pour l'édition d'un script
    */

    //---------------------------------------------------------------------------
    // Constantes


    //---------------------------------------------------------------------------
    // Variables globales
    var numTransport;
    var dateDepart;
    var dateArrivee;
    var idTemp;
    var temperatures;
    var minutesEcoulees = 0;

    var nombreTemperatures;
    var tabMinutes = [];
    var tabTemp = [];

    var tabVerifConformite = [];
    var maxTemp = 10;
    var maxTableau;

    var tabCalcMoy = [];
    var moyenneTemp;




    //---------------------------------------------------------------------------
    // Variables URL
    var urlRequeteAffichDetailTransport = "http://localhost/projet/REST/conteneur/recupTransportDetail/";
    var urlRequeteRecupTemp = "http://localhost/projet/REST/conteneur/recupTemperature/";

    //---------------------------------------------------------------------------
    // Programme principal avec jQuery
    $(document).ready(main) ;           // attente chargement du DOM

    function main() {                   // programme principal
        transport();
        $("#btnCreationPDF").hide();
        $("#btnCreationPDF").click(creationPDF);

    }
    //---------------------------------------------------------------------------
    // Fonctions évènementielles

    function transport() {
        $.ajax({
            type: "GET",
            url: urlRequeteAffichDetailTransport + numeroTransport,
            dataType: "jsonp",
            success: onGetTransport,
            error: onGetTransportError
        });
        function onGetTransport(reponse, status)
        {
                idTemp = reponse[0].idTemp;
                numTransport = reponse[0].numTransport;
                dateDepart = reponse[0].dateDepart;
                dateArrivee = reponse[0].dateArrivee;
                $("#numtransport").html(reponse[0].numTransport); // prendre le numéro de transport
                $("#datedepart").html(reponse[0].dateDepart); // prendre la date de depart avec l'heure
                $("#datearrivee").html(reponse[0].dateArrivee); // prendre la date d'arrivée avec l'heure
                $("#btnCreationPDF").show();
                $.ajax({
                    type: "GET",
                    url: urlRequeteRecupTemp + idTemp,
                    dataType: "jsonp",
                    success: onGetTemperature,
                    error: onGetTemperatureError
                });
                function onGetTemperature(reponse, status)
                {
                    nombreTemperatures = reponse.length;

                    //Vérification conformité températures
                    
                    reponse.forEach(function(element){  
                        tabVerifConformite.push(element.temp); // boucle pour récupérer la température
                    });
                    maxTableau = Math.max.apply(null, tabVerifConformite); 
                    if (maxTableau > maxTemp){  // si MaxTableau est superieur au maxTemp alors il a un probléme de transport si ce n'est pas le cas il aura aucun probléme
                        $("#divResult").append("<font color='red'>PROBLEME TRANSPORT - Il y a eu un dépassement de température</font><br>");
                    }else{
                        $("#divResult").append("<font color='green'>Aucun problème - Aucun dépassement de température</font><br>");
                    }

                    //Moyenne de toutes les températures récupérées
                    reponse.forEach(function(element){  
                        tabCalcMoy.push(element.temp);
                    });
                    var somme = 0;
                    for (var i = 0; i < tabCalcMoy.length; i++){
                        somme = somme + parseInt(tabCalcMoy[i]);
                    }
                    moyenneTemp = somme / tabCalcMoy.length;

                    $("#divResult").append("Il y a eu " + nombreTemperatures + " relevés de températures<br>Moyenne des températures : "+ moyenneTemp + "<br>");
        
                    //Affichage des températures dans un graphique
                    reponse.forEach(function(element){     
                        temperatures = element.temp;  
                        tabTemp.push(temperatures);
                        tabMinutes.push(minutesEcoulees + "min");
    
                        
                        var ctx = document.getElementById("myChart");
                        var myChart = new Chart(ctx, {
                            type: 'line',
                            data: {
                                labels: tabMinutes,
                                datasets: [{
                                    label: 'Températures',
                                    data: tabTemp
                                }]
                            }
                        });
        
                        minutesEcoulees = minutesEcoulees + 5; 
        
                    });
                    
                }
                function onGetTemperatureError(status)
                {
                    $("#codErr").html("Nous n'avons pas reussi a récuperer les conteneur");
                }
        }
        function onGetTransportError(status)
        {
            $("#codErr").html("Nous n'avons pas reussi a récuperer les transports");
        }

       
    }



    function creationPDF() {
        // Get the element.
        var element = document.getElementById('root');

        // Generate the PDF.
        html2pdf().from(element).set({
        margin: 1,
        filename: 'detailtransport-' + numTransport + '.pdf',
        html2canvas: { scale: 2 },
        jsPDF: {orientation: 'portrait', unit: 'in', format: 'letter', compressPDF: true}
        }).save();
    }



    //---------------------------------------------------------------------------
    // Fonctions utilitaires




    //---------------------------------------------------------------------------
    // Sources de données
