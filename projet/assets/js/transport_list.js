/* 
 * Fichier modèle pour l'édition d'un script
 */

//---------------------------------------------------------------------------
// Constantes


//---------------------------------------------------------------------------
// Variables globales


//---------------------------------------------------------------------------
// Variables URL
var urlRequeteAffichListTransport = "http://localhost/projet/REST/conteneur/recupTransportByContainer/";

//---------------------------------------------------------------------------
// Programme principal avec jQuery
$(document).ready(main) ;           // attente chargement du DOM

function main() {                   // programme principal
 
transport();
}
//---------------------------------------------------------------------------
// Fonctions évènementielles

function transport() {
    $.ajax({
        type: "GET",
        url: urlRequeteAffichListTransport + numeroContainer,
        dataType: "jsonp",
        success: onGetTransport,
        error: onGetTransportError
    });
    function onGetTransport(reponse, status)
    {
        reponse.forEach(function(element){      // // Boucle afin d'aller chercher les element voulu dans la base de données et les affichers dans la divResult
            $("#divResult").append("<tr>" + "<td>" + element.numTransport + "</td>" + '<td><a href="http://localhost/projet/C_transport/detail_transport/' + element.numTransport + '">Detail</a>' + "</td>" + "</tr>");
        });
        
        
    }
    function onGetTransportError(status)
    {
        $("#codErr").html("Nous n'avons pas reussi a récuperer les conteneur");
    }
}



//---------------------------------------------------------------------------
// Fonctions utilitaires




//---------------------------------------------------------------------------
// Sources de données
