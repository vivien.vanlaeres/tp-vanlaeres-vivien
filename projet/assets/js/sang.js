﻿﻿/* 
 * Fichier modèle pour l'édition d'un script
 */

//---------------------------------------------------------------------------
// Constantes


//---------------------------------------------------------------------------
// Variables globales
var identifiant = "";
var motdepasse = "";

//---------------------------------------------------------------------------
// Variables URL
var urlRequeteConnexion = "http://localhost/projet/REST/conteneur/verifConnexion/";
var urlRequeteAffichConteneur = "http://localhost/projet/REST/conteneur/recupContainers/";


//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Programme principal avec jQuery
$(document).ready(main) ;           // attente chargement du DOM

function main() {                   // programme principal
    $("#btnConnexion").click(ConnexionUtilisateur);
     conteneur(); 
}


//---------------------------------------------------------------------------
// Fonctions évènementielles

function ConnexionUtilisateur(){
    identifiant = $("#identifiant").val();
    motdepasse = $("#motdepasse").val();

    if((identifiant == "") && (motdepasse == "")){  //Si rien n'a été rempli dans les deux zones de saisis
        $("#codErr").html("Veuillez saisir votre identifiant et mot de passe");
    }
    if((identifiant == "") && (motdepasse != "")){  //Si rien n'a été rempli dans identifiant
        $("#codErr").html("Veuillez saisir votre identifiant");
    }
    if((identifiant != "") && (motdepasse == "")){  //Si rien n'a été rempli dans le mot de passe
        $("#codErr").html("Veuillez saisir votre mot de passe");
    }
    if((identifiant != "") && (motdepasse != "")){
    
        $.ajax({
            type: "GET",
            url: urlRequeteConnexion + identifiant,
            dataType: "jsonp",
            success: onGetIdentifiantConnexion,
            error: onGetIdentifiantConnexionError
        });
        function onGetIdentifiantConnexion(reponse, status)
        {
            if(reponse[0] == undefined){    //si l'identifiant n'est pas dans la bdd
                $("#codErr").html("Votre identifiant n'est pas connu");
            }
            if(reponse[0] != undefined){     //si l'identifiant est correct
                if((identifiant == reponse[0].login) && (motdepasse != reponse[0].password)){   //si le MP n'est pas correct
                    $("#codErr").html("Le mot de passe est incorrect");
                }
                if((identifiant == reponse[0].login) && (motdepasse == reponse[0].password)){   //si tout est OK!
                    window.location.href="C_conteneur"
                }
            }
        }
        function onGetIdentifiantConnexionError(status)
        {
            $("#codErr").html("Erreur lors de la récupération des identfiants");
        }
    }
}

function conteneur(){
    $.ajax({
        type: "GET",
        url: urlRequeteAffichConteneur,
        dataType: "jsonp",
        success: onGetConteneur,
        error: onGetConteneurError
    });
    function onGetConteneur(reponse, status)
    {
        reponse.forEach(function(element){     // Boucle afin d'aller chercher les element voulu dans la base de données et les affichers dans la divResult
            $("#divResult").append("<tr>" + "<td>" + element.numContainer + "</td>" + "<td>" + element.addrMac + "</td>"+"<td>" + element.etatContainer + "</td>"
            +"<td>" + '<a href="http://localhost/projet/C_transport/list_transport/' + element.numContainer + '">Transport</a>' + "</td>");
        });
        
        
    }
    function onGetConteneurError(status)
    {
        $("#codErr").html("Nous n'avons pas reussi a récuperer les conteneur");
    }
}



//---------------------------------------------------------------------------
// Fonctions utilitaires




//---------------------------------------------------------------------------
// Sources de données
